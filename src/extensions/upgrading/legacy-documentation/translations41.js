const Gettext = imports.gettext;
const St = imports.gi.St;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;


// Get all the Gettext functions, while aliasing `gettext()` as `_()`
const {
    gettext: _,
    ngettext,
    pgettext,
} = ExtensionUtils;


class Extension {
    enable() {
        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, Me.metadata.name, false);

        // Add an icon
        const icon = new St.Icon({
            icon_name: 'face-laugh-symbolic',
            style_class: 'system-status-icon',
        });
        this._indicator.add_child(icon);

        // Add the indicator to the panel
        Main.panel.addToStatusArea(Me.metadata.uuid, this._indicator);

        // A string needing more context is marked with `pgettext()`
        this._indicator.menu.addAction(pgettext('menu item', 'Notify'), () => {
            this._count += 1;

            // A regular translatable string is marked with the `_()` function
            const title = _('Notification');

            // A "countable" string is marked with the `ngettext()` function
            const body = ngettext('You have been notified %d time',
                'You have been notified %d times',
                this._count).format(this._count);

            Main.notify(title, body);
        });

        this._count = 0;
    }

    disable() {
        if (this._indicator) {
            this._indicator.destroy();
            this._indicator = null;
        }
    }
}


function init() {
    // If the `gettext-domain` key is not set in `metadata.json`, you must
    // pass the unique Gettext domain for your extension when initializing.
    ExtensionUtils.initTranslations(Me.metadata.uuid);

    return new Extension();
}
