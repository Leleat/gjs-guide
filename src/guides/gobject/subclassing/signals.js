import GObject from 'gi://GObject';

// #region example
const SignalsExample = GObject.registerClass({
    Signals: {
        'example-signal': {},
    },
}, class SignalsExample extends GObject.Object {
});
// #endregion example

// #region connection
const signalsExample = new SignalsExample();

// Connecting to the signal
const handlerId = signalsExample.connect('example-signal',
    () => console.log('example-signal emitted!'));

// Emitting the signal
signalsExample.emit('example-signal');

// Disconnecting from the signal
signalsExample.disconnect(handlerId);
// #endregion connection

/*
 * Default Handlers
 */
// #region defaults
const HandlerExample = GObject.registerClass({
    Signals: {
        'example-signal': {
            flags: GObject.SignalFlags.RUN_FIRST,
        },
    },
}, class HandlerExample extends GObject.Object {
    on_example_signal() {
        console.log('default handler invoked');
    }
});
// #endregion defaults

// #region emit-example
const handlerExample = new HandlerExample();

handlerExample.connect('example-handler',
    () => console.log('user handler invoked'));


/* Expected output:
 *   1. "default handler invoked"
 *   2. "user handler invoked"
 */
handlerExample.emit('example-signal');
// #endregion emit-example

/*
 * Detailed Signals
 */
// #region detailed
const DetailExample = GObject.registerClass({
    Signals: {
        'example-signal': {
            flags: GObject.SignalFlags.RUN_LAST | GObject.SignalFlags.DETAILED,
        },
    },
}, class DetailedExample extends GObject.Object {
    on_example_signal() {
        console.log('default handler invoked');
    }
});
// #endregion detailed

// #region detailed-connect
const detailExample = new DetailExample();

detailExample.connect('example-handler',
    () => console.log('user handler invoked'));

detailExample.connect('example-signal::foobar',
    () => console.log('user handler invoked (detailed)'));

detailExample.connect_after('example-signal',
    () => console.log('user handler invoked (after)'));

/* Expected output:
 *   1. "user handler invoked"
 *   2. "user handler invoked (detailed)"
 *   3. "default handler invoked"
 *   4. "user handler invoked (after)"
 */
detailExample.emit('example-signal::foobar');

/* Expected output:
 *   1. "user handler invoked"
 *   2. "default handler invoked"
 *   3. "user handler invoked (after)"
 */
detailExample.emit('example-signal::bazqux');
// #endregion detailed-connect

/*
 * Signal Parameters
 */
// #region parameters
const ParameterExample = GObject.registerClass({
    Signals: {
        'example-signal': {
            param_types: [GObject.TYPE_BOOLEAN, GObject.TYPE_STRING],
        },
    },
}, class ParameterExample extends GObject.Object {
});
// #endregion parameters

// #region parameters-connect
const parameterExample = new ParameterExample();

parameterExample.connect('example-signal', (emittingObject, arg1, arg2) => {
    console.log(`user handler invoked: ${arg1}, ${arg2}`);
});

// Expected output: "user handler invoked: true, foobar"
parameterExample.emit('example-signal', true, 'foobar');
// #endregion parameters-connect

/*
 * Signal Return Values
 */
// #region rvals
const ReturnExample = GObject.registerClass({
    Signals: {
        'example-signal': {
            return_type: GObject.TYPE_BOOLEAN,
        },
    },
}, class ReturnExample extends GObject.Object {
});
// #endregion rvals

// #region rvals-connect
const returnExample = new ReturnExample();

returnExample.connect('example-signal', () => {
    return true;
});

// Expected output: "signal handler returned true"
if (returnExample.emit('example-signal'))
    console.log('signal handler returned true');
else
    console.log('signal handler returned false');
// #endregion rvals-connect

/*
 * Signal Accumulator
 */
// #region accumulator
const AccumulatorExample = GObject.registerClass({
    Signals: {
        'example-signal': {
            flags: GObject.SignalFlags.RUN_LAST,
            accumulator: GObject.AccumulatorType.TRUE_HANDLED,
            return_type: GObject.TYPE_BOOLEAN,
        },
    },
}, class AccumulatorExample extends GObject.Object {
    on_example_signal() {
        console.log('default handler invoked');
        return true;
    }
});
// #endregion accumulator

// #region accumulator-connect
const accumulatorExample = new AccumulatorExample();

accumulatorExample.connect('example-signal', () => {
    console.log('first user handler');
    return false;
});

accumulatorExample.connect('example-signal', () => {
    console.log('second user handler');
    return true;
});

accumulatorExample.connect('example-signal', () => {
    console.log('third user handler');
    return true;
});

/* Expected output:
 *   1. "first user handler"
 *   2. "second user handler"
 */
accumulatorExample.emit('example-signal');
// #endregion accumulator-connect

