import GObject from 'gi://GObject';
import Gio from 'gi://Gio';


const icon = new Gio.ThemedIcon({name: 'dialog-information'});

if (icon.constructor.$gtype === Gio.ThemedIcon.$gtype)
    console.log('These values are exactly equivalent');

if (icon.constructor.$gtype === GObject.type_from_name('GThemedIcon'))
    console.log('These values are exactly equivalent');


if (icon instanceof Gio.ThemedIcon)
    console.log('instance is a GThemedIcon');

if (icon instanceof GObject.Object && icon instanceof Gio.Icon)
    console.log('instance is a GObject and GIcon');


const listStore = Gio.ListStore.new(Gio.Icon);

const pspec = GObject.ParamSpec.object(
    'gicon',
    'GIcon',
    'A property holding a GIcon',
    GObject.ParamFlags.READWRITE,
    Gio.Icon);
