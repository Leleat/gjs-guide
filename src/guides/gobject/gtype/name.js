import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';


/*
 * GTypeName
 */
const ExampleSubclass = GObject.registerClass({
}, class ExampleSubclass extends GObject.Object {
});

const objectInstance = new GObject.Object();
const subclassInstance = new ExampleSubclass();

// expected output: 'GObject'
console.log(GObject.Object.$gtype.name);
console.log(objectInstance.constructor.$gtype.name);

// expected output: 'Gjs_MySubclass'
console.log(ExampleSubclass.$gtype.name);
console.log(subclassInstance.constructor.$gtype.name);


/*
 * GtkBuilder
 */
const Square = GObject.registerClass({
    GTypeName: 'Square',
    Template: 'resource:///guide/gjs/Example/ui/square.ui',
}, class Square extends Gtk.Box {
});

const widget = new Square();

// expected output: 'Square'
console.log(Square.$gtype.name);
console.log(widget.constructor.$gtype.name);
