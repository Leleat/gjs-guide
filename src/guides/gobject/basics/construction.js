import Gtk from 'gi://Gtk?version=4.0';


Gtk.init();

const cancelLabel = new Gtk.Label({
    label: '_Cancel',
    use_underline: true,
});

const saveLabel = Gtk.Label.new_with_mnemonic('_Save');
