import GObject from 'gi://GObject';


// Create a new GValue
const booleanValue = new GObject.Value();

// Initialize it to hold a boolean
booleanValue.init(GObject.TYPE_BOOLEAN);


// Create a new GValue
const stringValue = new GObject.Value();
stringValue.init(GObject.TYPE_STRING);

// Set and get the value contents
stringValue.set_string('string value');
console.log(stringValue.get_string());


const doubleValue = new GObject.Value();
doubleValue.init(GObject.TYPE_DOUBLE);

if (GObject.type_check_value_holds(doubleValue, GObject.TYPE_DOUBLE))
    console.log('GValue initialized to hold double values');

if (!GObject.type_check_value_holds(doubleValue, GObject.TYPE_STRING))
    console.log('GValue not initialized to hold string values');
