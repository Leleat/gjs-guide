import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


/**
 * A simple Promise wrapper that elides the number of bytes read.
 *
 * @param {Gio.File} file - a file object
 * @returns {Promise<Uint8Array>} - the file contents
 */
function loadContents(file) {
    return new Promise((resolve, reject) => {
        file.load_contents_async(GLib.PRIORITY_DEFAULT, null, (_file, res) => {
            try {
                resolve(file.load_contents_finish(res)[1]);
            } catch (e) {
                reject(e);
            }
        });
    });
}

try {
    // A list of files to read
    const files = [
        Gio.File.new_for_path('test-file1.txt'),
        Gio.File.new_for_path('test-file2.txt'),
        Gio.File.new_for_path('test-file3.txt'),
    ];

    // Creating a Promise for each operation
    const operations = files.map(file => loadContents(file));

    // Run them all in parallel
    const results = await Promise.all(operations);

    results.forEach((result, i) => {
        console.log(`Read ${result.length} bytes from "${files[i].get_basename()}"`);
    });
} catch (e) {
    logError(e);
}
