import GLib from 'gi://GLib';


const loop = new GLib.MainLoop(null, false);

const idleId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 1, () => {
    console.log('idle source');

    return GLib.SOURCE_REMOVE;
});

const defaultId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
    console.log('default source');

    return GLib.SOURCE_REMOVE;
});

loop.run();
