import Gtk from 'gi://Gtk?version=4.0';

Gtk.init();


// This variable is in the top-level scope, effectively making it a global
// variable. It will trace whatever value is assigned to it, until it is set to
// another value or the script exits
let myLabel = null;

if (myLabel === null) {
    // This variable is only valid inside this `if` construct
    const myLabelScoped = new Gtk.Label({
        label: 'Some Text',
    });

    // After assigning `myLabelScoped` to `myLabel` the GObject is being
    // traced from two variables, preventing it from being collected or freed
    myLabel = myLabelScoped;
}

// The GObject is no longer being traced from `myLabelScoped`, but it is being
// traced from `myLabel` so it will not be collected.
console.log(myLabel.label);

// After we set `myLabel` to `null`, it will no longer be traceable from any
// variable, thus it will be collected and the GObject freed.
myLabel = null;
