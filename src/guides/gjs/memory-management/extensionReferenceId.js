import GLib from 'gi://GLib';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';


export default class ExampleExtension extends Extension {
    enable() {
        this._indicators = {};

        const indicator1 = new PanelMenu.Button(0.0, 'MyIndicator1', false);
        const indicator2 = new PanelMenu.Button(0.0, 'MyIndicator2', false);

        Main.panel.addToStatusArea(GLib.uuid_string_random(), indicator1);
        Main.panel.addToStatusArea(GLib.uuid_string_random(), indicator2);

        this._indicators['MyIndicator1'] = indicator1;
        this._indicators['MyIndicator1'] = indicator2;
    }

    disable() {
        for (const [name, indicator] of Object.entries(this._indicators))
            indicator.destroy();

        this._indicators = {};
    }
}
