import Gtk from 'gi://Gtk?version=4.0';

Gtk.init();


let myLabel = new Gtk.Label({
    label: 'Some Text',
});

// Once we add `myLabel` to `myFrame`, the GObject's reference count will
// increase and prevent it from being freed, even if it can not longer be
// traced from a JavaScript variable
const myFrame = new Gtk.Frame();
myFrame.set_child(myLabel);

// We can now safely stop tracing the GObject from `myLabel` by setting it
// to another value.
//
// It is NOT necessary to do this in most cases, as the variable will stop
// tracing the GObject when it falls out of scope.
myLabel = null;
