import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


// This method takes three arguments. Remember that JavaScript has no tuple
// type so we're using an Array instead.
const parameters = new GLib.Variant('(ssa{sv})', [
    'some-extension@someone.github.io',
    '',
    {},
]);

// This method has no return value, so the reply variant will be an empty tuple.
// You can also use this pattern to workaround the lack of `null` type in DBus.
const emptyReply = Gio.DBus.session.call_sync(
    'org.gnome.Shell',
    '/org/gnome/Shell',
    'org.gnome.Shell.Extensions',
    'OpenExtensionPrefs',
    parameters, // The method arguments
    null,       // The expected reply type
    Gio.DBusCallFlags.NONE,
    -1,
    null);


// This method takes no arguments. For convenience you can pass `null` instead
// of an empty tuple.
//
// This method returns a value. You may pass `GLib.VariantType` if you want the
// return value automatically type-checked.
const reply = Gio.DBus.session.call_sync(
    'org.gnome.Shell',
    '/org/gnome/Shell',
    'org.gnome.Shell.Extensions',
    'ListExtensions',
    null,                                // The method arguments
    new GLib.VariantType('(a{sa{sv}})'), // The expected reply type
    Gio.DBusCallFlags.NONE,
    -1,
    null);

// We know the first and only child of the tuple is the actual return value
const value = reply.get_child_value(0);

// Now we can unpack our value
const extensions = value.recursiveUnpack();
