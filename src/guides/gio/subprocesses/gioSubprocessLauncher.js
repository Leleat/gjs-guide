import Gio from 'gi://Gio';


const launcher = new Gio.SubprocessLauncher({
    flags: Gio.SubprocessFlags.STDIN_PIPE |
           Gio.SubprocessFlags.STDOUT_PIPE |
           Gio.SubprocessFlags.STDERR_PIPE,
});

// Set a custom ENV variable, which could be used in shell scripts
launcher.setenv('MY_VAR', '1', false);

// Log any errors to a file
launcher.set_stderr_file_path('error.log');

// Spawn as many processes with this launcher as you want
const proc1 = launcher.spawnv(['ls', '/']);
const proc2 = launcher.spawnv(['/home/me/script.sh']);
