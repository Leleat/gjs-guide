import Gio from 'gi://Gio';


const interfaceXml = `
<node>
  <interface name="guide.gjs.Test">
    <method name="SimpleMethod"/>
    <method name="ComplexMethod">
      <arg type="s" direction="in" name="input"/>
      <arg type="u" direction="out" name="length"/>
    </method>
    <signal name="TestSignal">
      <arg name="type" type="s"/>
      <arg name="value" type="b"/>
    </signal>
    <property name="ReadOnlyProperty" type="s" access="read"/>
    <property name="ReadWriteProperty" type="b" access="readwrite"/>
  </interface>
</node>`;


// Pass the XML string to create a proxy class for that interface
const TestProxy = Gio.DBusProxy.makeProxyWrapper(interfaceXml);


// Synchronous, blocking method
const proxySync = TestProxy(Gio.DBus.session, 'guide.gjs.Test',
    '/guide/gjs/Test');

// Asynchronous, non-blocking method (Promise-wrapped)
const proxyAsync = await new Promise((resolve, reject) => {
    TestProxy(
        Gio.DBus.session,
        'guide.gjs.Test',
        '/guide/gjs/Test',
        (proxy, error) => {
            if (error === null)
                resolve(proxy);
            else
                reject(error);
        },
        null, /* cancellable */
        Gio.DBusProxyFlags.NONE);
});


// Create a proxy synchronously, making sure to catch errors
let proxy = null;

try {
    proxy = TestProxy(Gio.DBus.session, 'guide.gjs.Test', '/guide/gjs/Test');
} catch (e) {
    logError(e, 'Constructing proxy');
}


// Properties work just like regular JavaScript object properties:
log(`ReadOnlyProperty: ${proxy.ReadOnlyProperty}`);
log(`ReadWriteProperty: ${proxy.ReadWriteProperty}`);

proxy.ReadWriteProperty = true;
log(`ReadWriteProperty: ${proxy.ReadWriteProperty}`);

// However, you will still have to watch Gio.DBusProxy::g-properties-changed to
// be notified of changes
proxy.connect('g-properties-changed', (_proxy, _changed, _invalidated) => {
});


// The wrapper function will assign both synchronous and asynchronous variants
// of methods on the object
proxy.SimpleMethodSync();

proxy.ComplexMethodRemote('input string', (returnValue, errorObj, fdList) => {
    // If @errorObj is `null`, then the method call succeeded and the variant
    // will already be unpacked with `GLib.Variant.prototype.deepUnpack()`
    if (errorObj === null) {
        console.log(`ComplexMethod('input string'): ${returnValue}`);

        if (fdList !== null) {
            // Methods that return file descriptors are fairly rare, so you
            // will know if you should expect one or not. Consult the API
            // documentation for `Gio.UnixFDList` for more information.
        }

        // If you were wrapping this function call in a Promise, this is where
        // you would call `resolve()`

    // If there was an error, then @returnValue will be an empty list and
    // @errorObj will be an Error object
    } else {
        logError(errorObj);

        // If you were wrapping this function call in a Promise, this is where
        // you would call `reject()`
    }
});


// Signals are connected and disconnected with the functions `connectSignal()`
// and `disconnectSignal()`, so they don't conflict with the GObject methods.
const handlerId = proxy.connectSignal('TestSignal', (_proxy, nameOwner, args) => {
    console.log(`TestSignal: ${args[0]}, ${args[1]}`);

    proxy.disconnectSignal(handlerId);
});
