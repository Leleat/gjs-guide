import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


// This is the most basic an action can be. It has a name and can be activated
// with no parameters, which results in the callback being invoked.
const basicAction = new Gio.SimpleAction({
    name: 'basicAction',
});

basicAction.connect('activate', (action, _parameter) => {
    console.log(`${action.name} activated!`);
});

// An action with a parameter
const paramAction = new Gio.SimpleAction({
    name: 'paramAction',
    parameter_type: new GLib.VariantType('s'),
});

paramAction.connect('activate', (action, parameter) => {
    console.log(`${action.name} activated: ${parameter.unpack()}`);
});

// And a stateful action. The state type is set at construction from the initial
// value, and can't be changed afterwards.
const stateAction = new Gio.SimpleAction({
    name: 'stateAction',
    state: GLib.Variant.new_boolean(true),
});

stateAction.connect('notify::state', (action, _pspec) => {
    console.log(`${action.name} changed: ${action.state.print(true)}`);
});


// Adding actions to an action group
const actionGroup = new Gio.SimpleActionGroup();
actionGroup.add_action(basicAction);
actionGroup.add_action(paramAction);
actionGroup.add_action(stateAction);


// Exporting action groups on the session bus
const connection = Gio.DBus.session;
const groupId = connection.export_action_group('/guide/gjs/Test',
    actionGroup);


// Once exported, action groups can be unexported using the returned ID
connection.unexport_action_group(groupId);
