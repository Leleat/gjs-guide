import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-directory');

const success = await file.make_directory_async(GLib.PRIORITY_DEFAULT,
    null);
