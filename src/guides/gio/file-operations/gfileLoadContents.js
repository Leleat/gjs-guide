import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-file.txt');

const [contents, etag] = await file.load_contents_async(null);

const decoder = new TextDecoder('utf-8');
const contentsString = decoder.decode(contents);
