---
title: Session Modes
---

# Session Modes

::: warning
This documentation is for GNOME 45 and later. Please see the
[Legacy Documentation][legacy-sessionmodes] for previous versions.
:::

Session modes are environment states of GNOME Shell. For example, when a user is
logged in and using their desktop the Shell is in the `user` mode.

Since GNOME 42, extensions have the option of operating in other session modes,
such as the `unlock-dialog` mode when the screen is locked. For more details,
see the [`session-modes`](/extensions/overview/anatomy.html#session-modes)
documentation.

[legacy-sessionmodes]: ../upgrading/legacy-documentation.md#session-modes

## Example Usage

Here is an example of a `metadata.json` for an extension that can run in the
regular `user` mode and in the `unlock-dialog` mode (when the screen is locked).

<<< @/../src/extensions/topics/session-modes/metadata.json{json}

Be aware that when the session mode changes between `user` and `unlock-dialog`,
GNOME Shell may call `disable()` and `enable()` on extensions. Extensions
should be prepared to handle `disable()` being called at any time.

Extensions that continue running on the lock screen may want to disable UI
elements when the session is locked, while continuing to operate in the
background. Note that GNOME Shell may use custom user modes other than `user`,
so the parent mode must be checked as well.


<<< @/../src/extensions/topics/session-modes/extension.js{js}
