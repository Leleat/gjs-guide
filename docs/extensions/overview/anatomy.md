---
title: Anatomy of an Extension
---
# Anatomy of an Extension

This document details the files and structure of a GNOME Shell extension. For
documentation on how to create and develop your first extension, see the
[Development](../#development) section of the extensions guide.

## Extension ZIP

Whether you're downloading from a repository (e.g. GitHub, GitLab) or installing
from the [GNOME Extensions Website], extensions are distributed as Zip files
with only two required files: `metadata.json` and `extension.js`.

Once unpacked and installed, the extension will be in one of two places:

```sh
# User Extension
~/.local/share/gnome-shell/extensions/example@gjs.guide/
    extension.js
    metadata.json

# System Extension
/usr/share/gnome-shell/extensions/example@gjs.guide/
    extension.js
    metadata.json
```

A more complete, zipped extension usually looks like this:

```
example@gjs.guide.zip
    locale/
        de/
          LC_MESSAGES/
              example.mo
    schemas/
        gschemas.compiled
        org.gnome.shell.extensions.example.gschema.xml
    extension.js
    metadata.json
    prefs.js
    stylesheet.css
```

The topic of GSettings and the `schemas/` directory is explained on the
[Preferences](../development/preferences.html) page.

The topic of Gettext and the `locale/` directory is explained on the
[Translations](../development/translations.html) page.


## `metadata.json` (Required)

`metadata.json` is a required file of every extension. It contains basic
information about the extension such as its UUID, name and description. Below is
a minimal example:

<<< @/../src/extensions/overview/anatomy/metadataRequired.json{json}

There are a number of other, optional fields that `metadata.json` may contain.
Below is a complete example, demonstrating all current possible fields:

<<< @/../src/extensions/overview/anatomy/metadata.json{json}

### Required Fields

#### `uuid`

This field is a globally-unique identifier for your extension, made of two parts
separated by `@`. Each part must only container letters, numbers, period (`.`),
underscore (`_`) and hyphen (`-`).

The first part should be a short string like "click-to-focus". The second part
must be some namespace under your control, such as `username.github.io`. Common
examples are `click-to-focus@username.github.io` and
`adblock@account.gmail.com`.

An extension's files must be installed to a folder with the same name as `uuid`
to be recognized by GNOME Shell:

```sh
~/.local/share/gnome-shell/extensions/example@gjs.guide/
```

#### `name`

This field should be a short, descriptive string like "Click To Focus",
"Adblock" or "Shell Window Shrinker".

#### `description`

This field should be a relatively short description of the extension's function.
If you need to, you can insert line breaks and tabs by using the `\n` and `\t`
escape sequences.

#### `shell-version`

This field is an array of strings describing the GNOME Shell versions that an
extension supports. It must include at least one entry or the extension will be
uninstallable.

For versions up to and including GNOME 3.38, this should have a major and minor
component such as `"3.38"`. Starting with GNOME 40, it should simply be the
major version, such as `"40"` or `"41"`.

Note that GNOME Shell has a configuration setting,
`disable-extension-version-validation`, which controls whether unsupported
extensions can be loaded. Before GNOME 40 this was `true` by default (users
could install extensions regardless of the `shell-version`), but because of the
major changes it is now `false` by default.

#### `url`

This field is a URL for an extension, which should almost always be a git
repository where the code can be found and issues can be reported.

It is required for extensions submitted to https://extensions.gnome.org/ to have
a valid URL.

### Optional Fields

#### `gettext-domain`

This field is a Gettext translation domain, used by GNOME Shell to
automatically initialize translations when an extension is loaded.

The domain should be unique to your extension and the easiest choice is to use
the UUID from your extension, such as `example@gjs.guide`.

Use of this field is optional and documented in the
[Translations](../development/translations.html) page.

#### `settings-schema`

This field is a [`Gio.SettingsSchema`] ID, used by `Extension.getSettings()`
and `ExtensionPreferences.getSettings()` methods to create a [`Gio.Settings`]
object for an extension.

By convention, the schema ID for extensions all start with the string
`org.gnome.shell.extensions` with the extension ID as a unique identifier, such
as `org.gnome.shell.extensions.example`.

Use of this field is optional and documented in the
[Preferences](../development/preferences.html) page.

#### `session-modes`

::: warning
This field was added in GNOME 42.
:::

This field is an array of strings describing the GNOME Shell session modes that
the extension supports. Almost all extensions will only use the `user` session
mode, which is the default if this field is not present.

::: tip
Extensions that specify `user` and `unlock-dialog` must still be prepared to
have `disable()` and `enable()` called when the session mode changes. 
:::

The current possible session modes are:

* `user`

  Extensions that specify this key run during active user sessions. If no other
  session modes are specified, the extension will be enabled when the session is
  unlocked and disabled when it locks.
    
* `unlock-dialog`

  Extensions that specify this key are allowed to run, or keep running, on the
  lock screen.
    
* `gdm`

  Extensions that specify this key are allowed to run, or keep running, on the
  login screen. This session mode is only available for system extensions that
  are enabled for the "gdm" user.
    
Extensions that want to support other session modes must provide a justification
to be approved during review for distribution from the GNOME Extensions website.

#### `version`

::: tip
See [`version-name`](#version-name) to set the version visible to users.
:::

This field is the submission version of an extension, incremented and
controlled by the GNOME Extensions website.

The value **MUST** be a whole number like `1`. It **MUST NOT** be a semantic
version like `1.1` or a string like `"1"`.

This field **SHOULD NOT** be set by extension developers. The GNOME Extensions
website will override this field and GNOME Shell may automatically upgrade an
extension with a lower `version` than the GNOME Extensions website.

#### `version-name`

This field sets the version visible to users. If not given the `version` field
will be displayed instead.

The value **MUST** be a string that only contains letters, numbers, space and
period with a length between 1 and 16 characters. It **MUST** contain at least
one letter or number.

A valid `version-name` will match the regex `/^(?!^[. ]+$)[a-zA-Z0-9 .]{1,16}$/`.

#### `donations`

This field is an object including donation links with these possible keys:

- `buymeacoffee`
- `custom`
- `github`
- `kofi`
- `liberapay`
- `opencollective`
- `patreon`
- `paypal`

Value of each element can be string or array of strings (maximum array
length is 3).

While `custom` pointing to the exact value (URL), other keys only including
the user handle (for example, `"paypal": "john_doe"` points to the
`https://paypal.me/john_doe`).

## `extension.js` (Required)

::: warning
GNOME Shell and Extensions use ESModules as of GNOME 45. Please see the
[Legacy Documentation][legacy-extension] for previous versions.
:::

`extension.js` is a required file of every extension. It must export a subclass
of the base `Extension` and implement the `enable()` and `disable()` methods. If
your subclass overrides the `constructor()` method, it must also call `super()`
and pass the `metadata` argument to the parent class.

<<< @/../src/extensions/overview/anatomy/extension.js{js}

[legacy-extension]: ../upgrading/legacy-documentation.md#extension

### `ExtensionMetadata` Object

The [`ExtensionMetadata`] object is passed to the `constructor()` of the
`Extension` class and (`ExtensionPreferences` class) when loaded, and available
as the `metadata` property afterwards.

This object is described in more detail on the [Extension (ESModule)] topic
page.

[`ExtensionMetadata`]: ../topics/extension.md#extensionmetadata
[Extension (ESModule)]: ../topics/extension.md

## `prefs.js`

::: warning
GNOME Shell and Extensions use ESModules as of GNOME 45. Please see the
[Legacy Documentation][legacy-preferences] for previous versions.
:::

`prefs.js` is used to build the preferences for an extensions. If this file is
not present, there will simply be no preferences button in GNOME Extensions or
the [GNOME Extensions Website](https://extensions.gnome.org/local/).

<<< @/../src/extensions/overview/anatomy/prefs.js{js}

Something that's important to understand:

* The code in `extension.js` is executed in the same process as `gnome-shell`
 
  Here you **will** have access to live code running in GNOME Shell, but fatal
  errors or mistakes will affect the stability of the desktop. It also means you
  will be using the [Clutter] and [St] toolkits.
  
* The code in `prefs.js` will be executed in a separate Gtk process

  Here you **will not** have access to code running in GNOME Shell, but fatal
  errors or mistakes will be contained within that process. In this process you
  will be using the [GTK4] and [Adwaita] toolkits.

You can open the preferences dialog for your extension manually with
`gnome-extensions prefs`:

```sh
$ gnome-extensions prefs example@gjs.guide
```

[legacy-preferences]: ../upgrading/legacy-documentation.md#preferences

## `stylesheet.css`

::: tip
The CSS in this file will only apply to GNOME Shell and extensions, not the
extension preferences or any other application.
:::

`stylesheet.css` is CSS stylesheet which can apply custom styles to your widgets
in `extension.js` or GNOME Shell as a whole. For example, if you had the
following widgets:

<<< @/../src/extensions/overview/anatomy/stylesheet.js{js}

You could have this in your `stylesheet.css`:

<<< @/../src/extensions/overview/anatomy/stylesheet.css{css}

[GNOME Extensions Website]: https://extensions.gnome.org
[`Gio.Settings`]: https://gjs-docs.gnome.org/gio20/gio.settings
[`Gio.SettingsSchema`]: https://gjs-docs.gnome.org/gio20/gio.settingsschema
[Clutter]: https://gjs-docs.gnome.org/#q=clutter
[St]: https://gjs-docs.gnome.org/#q=st
[GTK4]: https://gjs-docs.gnome.org/gtk40
[Adwaita]: https://gjs-docs.gnome.org/adw1

