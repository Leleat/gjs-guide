---
title: Tips On Memory Management
---

# Tips On Memory Management

GJS is JavaScript bindings for GNOME, which means that behind the scenes there
are two types of memory management happening: reference tracing (JavaScript)
and reference counting (GObject).

Most developers will never have to worry about GObject referencing or memory
leaks, especially if writing clean, uncomplicated code. This page describes
some common ways developers fail to take scope into account or cleanup main
loop sources and signal connections.


## Basics

The concept of reference counting is very simple. When a GObject is first
created, it has a reference count of `1`. When the reference count drops to `0`,
all the object's resources and memory are automatically freed.

The concept of reference tracing is also quite simple, but can get confusing
because it relies on external factors. When a value or object is no longer
assigned to any variable, it will be garbage collected. In other words, if the
JavaScript engine can not "trace" a value back to a variable it will free that
value.

Put simply, as long as GJS can trace a GObject to a variable, it will ensure
the reference count does not drop to `0`.


### Relevant Reading

If you are very new to programming, you should familiarize yourself with the
concept of **scope** and the three types of variables in JavaScript: `const`,
`let` and `var`. A basic understanding of these will help you a lot while trying
to ensure the garbage collector can do its job.

You should also consider enabling **strict mode** in all your scripts, as this
will prevent some mistakes that lead to uncollectable objects.

 * [`const`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/const),
   [`let`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/let),
   [`var`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/var)
 * [Variable Scope](https://developer.mozilla.org/docs/Glossary/Scope)
 * [Strict mode](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Strict_mode)


### Examples

Below we create a `GtkLabel` and assign it to the variable `myLabel`. By assigning
it to a variable we are "tracing" a reference to it, preventing the JS Object
from being garbage collected and the GObject from being freed:

<<< @/../src/guides/gjs/memory-management/tracingValue.js{js}

Because `let` has block scope, `myLabel` would stop being traced when the
scope is left. If the GObject is not assigned to a variable in a parent scope,
it will be collected and freed.

<<< @/../src/guides/gjs/memory-management/tracingScope.js{js}

Other GObjects can hold a reference to GObjects, such as a container object.
This means that even if it can not be traced from a JavaScript variable, it
will have a positive reference count and not be freed.

<<< @/../src/guides/gjs/memory-management/tracingGObject.js{js}

However, if the only thing preventing a GObject from being collected is another
GObject holding a reference, once it drops that reference it will be freed.

<<< @/../src/guides/gjs/memory-management/tracingGObjectUnref.js{js}

## Use After Free

Most functions that affect memory management are not available in GJS, but
there are some exceptions. Functions like `Clutter.Actor.destroy()` found in
some libraries will force a GObject to be freed as though its reference count
had dropped to `0`. What it won't do is stop a JavaScript variable from tracing
that GObject.

Attempting to access a GObject after it has been finalized (freed) is a
programmer's error that is not uncommon to see in extensions. Trying to use
such an object will raise a critical error and print a stack trace to the log.
Below is a simple example of how you can continue to trace a reference to a
GObject that has already been freed:

<<< @/../src/guides/gjs/memory-management/tracingDestroy.js{js}

## Leaking References

Although memory is managed for you by GJS, remember that this is done by
tracing to variables. If you lose track of a variable, the ID for a signal or
source callback you will leak that reference.

### Scope

The easiest way to leak references is to overwrite a variable or let it fall
out of scope. If this variable points to a GObject with a positive reference
count that you are responsible for freeing, you will effectively leak its
memory.

The following example demonstrates how reference leaks often occur in
[GNOME Shell Extensions](../../extensions/). When the `enable()` function
returns the ID to the `indicator` object is collected, and we have lost our
ability to destroy the object when `disable()` is called.

<<< @/../src/guides/gjs/memory-management/extensionReferenceScope.js{js}

Here's another example. Although the principle of the code below is
sound, we are leaking a reference to a GObject we are responsible for (and thus
memory). The leak below is fairly easy to spot; what's important is how and why
that reference was leaked. Mistakes like these are often easier to make and
harder to track down.

<<< @/../src/guides/gjs/memory-management/extensionReferenceId.js{js}

::: details Having trouble to find the leak? 
Even though the author of the code above had an intention of storing references
to all the indicators in the `indicators` object, they made a small mistake. The
line 19 stores the reference to `indicator2` at `indicators['MyIndicator1']`,
overwriting the reference to `indicator1`! With that, the reference to the
object under `indicator1` is lost, and the object cannot be freed in
`disable()`.
:::

### Main Loop Sources

A very common way of leaking GSource's is recursive (repeating) sources added
to the GLib event loop. These are usually repeating timeout loops, used to
update something in the UI.

In the example [GNOME Shell Extension](https://gjs.guide/extensions/) below,
the ID required to remove the `GSource` from the main loop has been lost and
the callback will continue to be invoked even after the object has been
destroyed.

So in this case, the leak is caused by the main loop holding a reference to the
`GSource`, while the programmer has lost their ability to remove it. When the
source callback is invoked, it will try to access the object after it has been
destroyed, causing a critical error.

<<< @/../src/guides/gjs/memory-management/leakEventSource.js{js}

### Signal Callbacks

Like main loop sources, whenever a signal is connected it returns a handler ID.
Failing to remove sources or disconnect signals can result in "use-after-free"
scenarios, but it's also possible to trace variables and leak references in a
callback.

In the example below, the callback for the `changed::licensed` signal is
tracing the `agent` dictionary, which is why the callback continues to work
after `constructor()` returns. In fact, even after we set `myObject` to `null`
in `disable()`, the dictionary is still be traced from within the signal
callback.

<<< @/../src/guides/gjs/memory-management/leakSignalClosure.js{js}

## Cairo

Cairo is an exception in GJS, in that it is necessary to manually free the
memory of a `CairoContext` at the end of a drawing method. This is simple to
do, but forgetting to do it can leak a lot of memory because widgets can be
redrawn quite often.

<<< @/../src/guides/gjs/memory-management/leakCairo.js{js}

