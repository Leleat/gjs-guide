---
title: Asynchronous Programming
---

# Asynchronous Programming

JavaScript is a single-threaded, concurrent programming language. Concurrency is
achieved by using an event loop that halts while processing an event, before
returning to process more events.

The [`Promise`][promise] API is a powerful framework for controlling execution
flow in an event loop, while keeping code simple and maintainable.

Although JavaScript can only be run in a single-thread environment, the GNOME
APIs contain many functions that use per-task threads to execute blocking
operations.

Using these three tools together allows GJS programmers to schedule events based
on their priority, keep code clean and understandable, and indirectly use
threads to write responsive and performant code.

[promise]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise


## The Main Loop

In order to process events in a concurrent fashion, a JavaScript engine needs an
event loop. Although GJS is powered by Firefox's [SpiderMonkey][spidermonkey]
JavaScript engine, it uses GLib's [Event Loop][mainloop]. The event loop is the
foundation of concurrent and asynchronous programming in GJS, so we will cover
it in some detail.

If you are writing an application using [`Gio.Application`][gapplication] or one
of its subclasses like `Gtk.Application` or `Adw.Application`, a main loop will
be started for you when you call [`Gio.Application.run()`][gapplicationrun]. If
you are writing a GNOME Shell Extension, you will be using the main loop already
running in GNOME Shell.

It's still useful to know how to create a main loop for simple scripts, so let's
get started with an example of creating a `GLib.MainLoop` and adding a timeout
source:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoop.js{js}

If you run that example as a script it would never exit, because the loop is
never instructed to quit. The example below will iterate the main context for
one second, before instructing the loop to quit:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopQuit.js{js}

[spidermonkey]: https://spidermonkey.dev
[mainloop]: https://docs.gtk.org/glib/main-loop.html
[gapplication]: https://gjs-docs.gnome.org/gio20/gio.application
[gapplicationrun]: https://gjs-docs.gnome.org/gio20/gio.application#method-run


### Event Sources

There are many types of sources in GLib and you may even create your own, but the
two most common you will create explicitly are timeout sources and idle sources:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopIdleTimeout.js{js}

Sources can also be created implicitly, like when calling the asynchronous
methods found in GIO. These functions usually execute tasks in a background
thread, then once they complete add a `GLib.Source` to the caller's main context
to invoke a callback:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopGTask.js{js}

Here's one more, slightly more advanced example. In this case, we'll create a
input stream for `stdin` (that's the stream you use when you type in a terminal)
then use it to create a source that triggers when the user presses `Enter`:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopSocket.js{js}

### Event Priority

Each event source will have a priority, to determine which will get "dispatched"
if more than one is ready at the same time. Priorities are simply positive or
negative `Number` values; the lower the number the higher the priority.

Consider the table of common priorities below. In particular, notice how
`Gtk.PRIORITY_RESIZE` has a higher priority than `Gdk.PRIORITY_REDRAW` so that a
`Gtk.Window` isn't redrawn for every little step resizing:


| Constant                     | Value |
|------------------------------|-------|
| `GLib.PRIORITY_LOW`          |  300  |
| `GLib.PRIORITY_DEFAULT_IDLE` |  200  |
| `Gdk.PRIORITY_REDRAW`        |  120  |
| `Gtk.PRIORITY_RESIZE`        |  110  |
| `GLib.PRIORITY_HIGH_IDLE`    |  100  |
| `GLib.PRIORITY_DEFAULT`      |    0  |
| `GLib.PRIORITY_HIGH`         | -100  |

In the example below, we will add two timeout sources that resolve at the same
time, but with differing priorities so that one is dispatched first:

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopPriority.js{js}

### Removing Sources

There are two ways you can remove a source from the loop, if it was added with
`GLib.timeout_add()` or `GLib.idle_add()`.

Both of these functions return an opaque value, just like a signal connection.
This value can be passed to [`GLib.Source.remove()`][gsourceremove], after which
the source will be removed from the main context and the callback will not be
invoked.

The other way depends on the return value of the source callback. If the
callback returns `GLib.SOURCE_CONTINUE` the callback will be invoked again when
the source's condition is met. If it returns `GLib.SOURCE_REMOVE`, the source
will be removed and the callback will never be invoked again.

<<< @/../src/guides/gjs/asynchronous-programming/glibMainLoopSourceRemoval.js{js}

Other event sources, like those created by asynchronous methods in GIO, can not
be removed directly. Most operations may be cancelled however, by passing a
[`Gio.Cancellable`][gcancellable].


[gsourceremove]: https://gjs-docs.gnome.org/glib20/glib.source#function-remove
[gcancellable]: https://gjs-docs.gnome.org/gio20/gio.cancellable


## Promises

In GJS, a [`Promise`][mdnpromise] is basically an event source that triggers
when the `resolve()` or `reject()` functions are invoked. If you are new to
`Promise` and `async` in JavaScript, you should review the following articles on
MDN:

* [Using promises](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Using_promises)
* [`async`/`await`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/async_function)

This section will only briefly cover `Promise` usage in JavaScript and GJS.

[mdnpromise]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise


### Traditional Usage

<<< @/../src/guides/gjs/asynchronous-programming/promiseCallback.js{js}

### `async`/`await`

Although `Promise` objects offer a simple API for asynchronous operations, they
still have the burden of callbacks.

With the `async`/`await` paradigm, programmers can regain the simplicity of
synchronous programming, with the benefits of asynchronous execution.

<<< @/../src/guides/gjs/asynchronous-programming/promiseAwait.js#snippet{3 js}


## Asynchronous Operations

While you can not run JavaScript on multiple threads, almost all GIO operations
have asynchronous variants. These work by collecting the necessary input in the
function arguments, sending the work to be done on another thread, then they
invoke a callback in the main thread when finished.


### Traditional Usage

Although written in JavaScript, the example below is how a C programmer will
typically use these functions.

While this won't block the main thread for the duration of the operation, it is
awkward compared to the modern programming styles usually used in high-level
languages like JavaScript.

<<< @/../src/guides/gjs/asynchronous-programming/gtaskCallback.js{js}

### `async`/`await`

One of the most convenient uses of `Promise` for GJS programmers, is to wrap
these asynchronous functions and use the `async`/`await` pattern to regain
synchronous flow:

<<< @/../src/guides/gjs/asynchronous-programming/gtaskAwait.js{js}

The important thing to notice is that by using the `async`/`await` pattern, you
can maintain a simple, synchronous-like programming style while taking advantage
of asynchronous execution.

With a wrapper function prepared, you can even run many of these operations in
parallel; each in its own thread:

<<< @/../src/guides/gjs/asynchronous-programming/gtaskAwaitParallel.js{js}

### Promisify Helper

In GJS 1.54 a convenient helper was added as a technology preview, based on the
work of Outreachy intern [Avi Zajac][avizajac]. Ultimately, GJS will have
seamless support for async functions, but until then you can use the
"promisify" helper to automatically create `Promise` wrappers.

The `Gio._promisify()` utility replaces the original function on the class
prototype, so that it can be called on any instance of the class, including
subclasses. Simply pass the class prototype, the "async" function name and the
"finish" function name as arguments:

<<< @/../src/guides/gjs/asynchronous-programming/gtaskPromisify.js#snippet{5-6 js}


The function may then be used like any other `Promise` without the need for a
custom wrapper, simply by leaving out the callback argument:

<<< @/../src/guides/gjs/asynchronous-programming/gtaskPromisify.js#snippet{9-16 js}


The original function will still be available, and can be used simply by passing
the callback:

<<< @/../src/guides/gjs/asynchronous-programming/gtaskPromisify.js#snippet{18-29 js}


[avizajac]: https://llzes.org/


### Cancelling Operations

One benefit of GIO's asynchronous methods, is the ability to cancel them after
they have started. This is done by using a [`Gio.Cancellable`][gcancellable]
object, which can be triggered from any thread.

<<< @/../src/guides/gjs/asynchronous-programming/gtaskCancellable.js{js}

You may pass the same `Gio.Cancellable` object to as many operations as you
want, and cancel them all with a single call to `Gio.Cancellable.cancel()`. This
can be useful if many operations depend on a single state, such as a GNOME Shell
Extension being enabled or disabled.

Once a `Gio.Cancellable` has been cancelled, you should drop the reference to it
and create a new instance for future operations.

[gcancellable]: https://gjs-docs.gnome.org/gio20/gio.cancellable

