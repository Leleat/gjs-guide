---
title: Intro
---

# GNOME JavaScript Introduction

Welcome to GNOME JavaScript (GJS)! This introduction covers the basics of
how JavaScript is used with GJS. More detailed information about GJS is
available in the [GJS Usage Documentation].

[GJS Usage Documentation]: https://gjs-docs.gnome.org/gjs

## Platform and API

Many APIs that JavaScript developers are familiar with are actually a part of
the [Web API], such as DOM or Fetch. GJS is not intended as an environment for
web development and does not include support for most of these APIs.

[Web API]: https://developer.mozilla.org/docs/Web/API

### GNOME Platform

::: tip
GNOME Platform APIs are documented at [gjs-docs.gnome.org](https://gjs-docs.gnome.org)
:::

GJS provides JavaScript bindings for the GNOME platform libraries, meaning that
developers will use libraries like [`Gio`] for working with files, [`Gtk`] to
build user interfaces, [`Soup`] for WebSockets and so on.

[`Gio`]: https://gjs-docs.gnome.org/gio20
[`Gtk`]: https://gjs-docs.gnome.org/gtk40
[`Soup`]: https://gjs-docs.gnome.org/soup30

### Web APIs

::: tip
See the documentation on [GJS Usage](https://gjs-docs.gnome.org/gjs/) for
details about built-in modules and APIs.
:::

Since the adoption of ES Modules, GJS has gained support for some widely used
standards from the [Web API]. These APIs may be more familiar to developers
that have used other JavaScript environments.

#### Console API

As of GJS 1.70 (GNOME 41), the Console API is available as described in the
[WHATWG Console Standard][console-standard]. The `console` object is available
globally without import.

[console-standard]: https://console.spec.whatwg.org/

#### Encoding API

As of GJS 1.70 (GNOME 41), the Encoding API is available as described in the
[WHATWG Encoding Standard][encoding-standard]. The `TextDecoder` and
`TextEncoder` objects are available globally without import.

[encoding-standard]: https://encoding.spec.whatwg.org/

#### Timers API

As of GJS 1.70 (GNOME 41), the Timers API is available as described in the
[WHATWG HTML Standard][html-standard]. The `setTimeout()`, `setInterval()`,
`clearTimeout()` and `clearInterval()` methods are available globally without
import.

[html-standard]: https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#timers

## Imports and Modules

::: tip
See the [Extensions Documentation](../../extensions/overview/imports-and-modules.md)
for ES Module usage in GNOME Shell Extensions.
:::

GJS uses standard ES Modules for imports and modules, but includes additional
specifiers for platform libraries like `Gtk`. 

### ES Modules

::: tip
See the [GJS Documentation](https://gjs-docs.gnome.org/gjs/esmodules.md) for
details about using ES Modules in GJS.
:::

As of GJS 1.68 (GNOME 40), standard [ES Modules] are supported and the
preferred import method.

<<< @/../src/guides/gjs/intro/modules.js{js:no-line-numbers}

[ES Modules]: https://developer.mozilla.org/docs/Web/JavaScript/Guide/Modules

### Legacy Imports

::: warning
ES Modules should be preferred for new projects and existing projects should
migrate when possible.
:::

Prior to the ECMAScript standard for modules, GJS used a custom `imports`
object.

<<< @/../src/guides/gjs/intro/legacyModules.js{js:no-line-numbers}

[ES Modules]: https://developer.mozilla.org/docs/Web/JavaScript/Guide/Modules

## Classes

GJS uses standard JavaScript syntax for classes when possible, but includes
additional syntax for GObject classes.

### ES Classes

GJS uses standard [ES Classes] for JavaScript classes.

<<< @/../src/guides/gjs/intro/classes.js{js:no-line-numbers}

[ES Classes]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Classes

### GObject Classes

::: tip
See the documentation on [GObject Basics](../gobject/basics.md) and
[GObject Subclassing](../gobject/subclassing.md) for more details.
:::

The GNOME platform is built on the GObject type system, which brings
object-oriented concepts to C. GJS uses special syntax to integrate with GObject.

<<< @/../src/guides/gobject/subclassing/object.js{js:no-line-numbers}

### Legacy Classes

::: warning
ES Classes should be preferred for new projects and existing projects should
migrate when possible.
:::

Prior to the ECMAScript standard for classes, GJS used a custom syntax to
integrate with GObject.

<<< @/../src/guides/gjs/intro/legacyClasses.js{js:no-line-numbers}
