---
title: The Basics
---

# The Basics of GObject

GObject is the base upon which most of the GNOME platform is built. This is a
gentle introduction to using GObject in GJS, including constructing objects,
using properties and connecting to signals.

## GObject Construction

::: tip
In rare cases, like the [`Gio.File`] interface, objects can not be constructed
with the `new` operator and a constructor method must always be used.
:::

The most common way to create a new GObject is using the [`new`] operator. When
constructing a GObject this way, you can pass a dictionary of properties:

<<< @/../src/guides/gobject/basics/construction.js#snippet{6-9 js:no-line-numbers}


Many classes also have static constructor methods you can use directly:

<<< @/../src/guides/gobject/basics/construction.js#snippet{11-11 js:no-line-numbers}


[`new`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/new
[`Gio.File`]: https://gjs-docs.gnome.org/gio20/gio.file

## Properties

GObject supports a property system that is slightly more powerful than native
JavaScript properties.

### Accessing Properties

GObject properties may be retrieved and set using native property style access
or GObject get and set methods.

<<< @/../src/guides/gobject/basics/properties.js#snippet{8-20 js:no-line-numbers}


GObject property names have a canonical form that is `kebab-cased`, however they
are accessed differently depending on the situation:

<<< @/../src/guides/gobject/basics/properties.js#snippet{23-39 js:no-line-numbers}


### Property Change Notification

Most GObject properties will emit [`GObject.Object::notify`] when the value is
changed (more on [signals](#signals) below). You can connect to this signal in
the form of `notify::property-name` to invoke a callback when it changes:

<<< @/../src/guides/gobject/basics/properties.js#snippet{42-46 js:no-line-numbers}


[`GObject.Object::notify`]: https://gjs-docs.gnome.org/gobject20/gobject.object#signal-notify

### Property Bindings

GObject provides a simple way to bind a property between objects, which can be
used to link the state of two objects. The direction and behavior can be
controlled by the [`GObject.BindingFlags`] passed when the binding is created.

<<< @/../src/guides/gobject/basics/properties.js#snippet{49-63 js:no-line-numbers}


If you need to transform the value between the source and target, you can use
[`GObject.Object.bind_property_full()`].

<<< @/../src/guides/gobject/basics/properties.js#snippet{66-74 js:no-line-numbers}


[`GObject.BindingFlags`]: https://gjs-docs.gnome.org/gobject20/gobject.bindingflags
[`GObject.Object.bind_property_full()`]: https://gjs-docs.gnome.org/gobject20/gobject.object#method-bind_property_with_closures

## Signals

GObjects support a signaling system, similar to events and EventListeners in the
JavaScript Web API. Here we will cover the basics of connecting and
disconnection signals, as well as using callbacks.

### Connecting Signals

::: tip
When a GObject is destroyed, all signal connections are destroyed with it.
:::

Signals are connected by calling [`GObject.Object.prototype.connect()`], which
returns a handler ID that is always truthy. Signals are disconnected by passing
that ID to [`GObject.Object.prototype.disconnect()`]:

<<< @/../src/guides/gobject/basics/signals.js#snippet{7-16 js:no-line-numbers}


[`GObject.Object.prototype.connect()`]: https://gjs-docs.gnome.org/gjs/overrides.md#gobject-object-connect
[`GObject.Object.prototype.disconnect()`]: https://gjs-docs.gnome.org/gjs/overrides.md#gobject-object-disconnect

### Callback Arguments

Signals often have multiple callback arguments, but the first is always the
emitting object:

<<< @/../src/guides/gobject/basics/signals.js#snippet{19-30 js:no-line-numbers}


### Callback Return Values

::: warning
A callback with no return value will implicitly return `undefined`, while
an `async` function will implicitly return a `Promise`.
:::

Some signals expect a return value, usually a `boolean`. The type and behavior
of the return value will be described in the documentation for the signal.

<<< @/../src/guides/gobject/basics/signals.js#snippet{33-45 js:no-line-numbers}


Using an `async` function as a signal handler will return an implicit `Promise`,
which will be coerced to a truthy value. If necessary, use a traditional
`Promise` chain and return the expected value type explicitly.

<<< @/../src/guides/gobject/basics/signals.js#snippet{47-52 js:no-line-numbers}

