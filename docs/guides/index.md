---
title: Guides
---

# Developer Guides

::: tip
[Workbench](https://apps.gnome.org/Workbench/) is an interactive tool
with over a hundred JavaScript demos of the GNOME platform.
:::

## GJS

> Welcome to GJS!

### [Introduction](gjs/intro.md)

How does GJS compare to other JavaScript environments?

### [Asynchronous Programming](gjs/asynchronous-programming.md)

How does GJS combine an event loop, the `Promise` API and task threads?

### [Style Guide](gjs/style-guide.md)

The official style guide for GJS and GNOME projects written in GJS.

### [Tips On Memory Management](gjs/memory-management.md)

Tips on memory management in GJS.

## GLib

> A low-level utility library

### [GVariant](glib/gvariant.md)

A memory efficient data format used in D-Bus, GSettings and more

## GObject

> Getting to know GObject

### [The Basics](gobject/basics.md)

A gentle introduction to GObject

### [Subclassing](gobject/subclassing.md)

How to subclass and extend GObject

### [Interfaces](gobject/interfaces.md)

How to implement GObject Interfaces

### [GType](gobject/gtype.md)

The foundation of the GObject system

### [GValue](gobject/gvalue.md)

A generic value container, rarely exposed in JavaScript

### [Advanced](gobject/advanced.md)

Advanced usage of GObject, properties and signals.

## Gio

> Looking for help with files or application settings?

### [Actions and Menus](gio/actions-and-menus.md)

D-Bus inter-process communication in GJS

### [D-Bus](gio/dbus.md)

D-Bus inter-process communication in GJS

### [File IO in GJS](gio/file-operations.md)

Basic File Operations in GJS

### [List Models](gio/list-models.md)

List model API implementation and usage

### [Subprocesses in GJS](gio/subprocesses.md)

Spawning subprocesses in GJS

## GTK

> Looking for help building an application?

### [Building an Application](gtk/3/)

New to GTK or using it with GJS? This is for you!

### [Application Packaging](gtk/application-packaging.md)

How do I package my GTK application with GJS?

